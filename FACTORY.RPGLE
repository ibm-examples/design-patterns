**Free

// Tworzenie obiekt�w bez okre�lania konkretnych klas
 ctl-opt dftactgrp(*no) actgrp('MYAPP') main(Main);

 dcl-ds Product qualified ;
   // Dane produktu
    name char(20) ;
 end-ds;

 dcl-proc Main;

        CreateProduct('TypeA') ;

        return;
 end-proc;

 dcl-proc CreateProduct export;
   dcl-pi *n;
      ProductType char(10) value;
   end-pi;

   select;
      when (ProductType = 'TypeA');
         // Logika tworzenia produktu typu A
         Product.name = 'ProductA' ;
         dsply 'ProductA' ;
      when (ProductType = 'TypeB');
         // Logika tworzenia produktu typu B
         Product.name = 'ProductB' ;
          dsply 'ProductB' ;
      other;
         // Obs�uga nieznanego typu
          dsply 'product unknown' ;
   endsl;
end-proc;
