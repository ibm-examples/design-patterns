**free

  ctl-opt dftactgrp(*no) actgrp('MYAPP') main(Main) ;

  dcl-c ACTIVE const(1) ;
  dcl-c INACTIVE const(2) ;
  dcl-c PENDING const(3) ;

  dcl-ds StatusMap qualified ;
     Active int(5) inz(1) ;
     Inactive int(5) inz(2) ;
     Pending int(5) inz(3) ;
     Arr int(5) dim(3) pos(1) ;
  end-ds;

  dcl-proc Main ;

        dcl-s status int(5) inz(INACTIVE) ;

        EnumAsConstNumberWithSelect(status) ;
        EnumAsConstMap(status) ;

        return ;
  end-proc ;

 dcl-proc EnumAsConstMap ;
   dcl-pi *n;
    status int(5);
   end-pi;

   dcl-s position int(3) ;

   dcl-s enumerate varchar(20) dim(3) ;
   enumerate(ACTIVE) = 'Status: ACTIVE' ;
   enumerate(INACTIVE) = 'Status: INACTIVE' ;
   enumerate(PENDING) = 'Status: PENDING' ;

   position = %lookup( status: StatusMap.Arr) ;

   if (position > 0) and (position <= %elem(enumerate)) ;
     snd-msg enumerate(position) ;
   else ;
     snd-msg 'Nieznany status' ;
   endif ;

 end-proc;

 dcl-proc EnumAsConstNumberWithSelect ;
   dcl-pi *n;
    status int(5);
   end-pi;

   select;
     when (status = ACTIVE) ;
        snd-msg 'Status: ACTIVE' ;
     when (status = INACTIVE) ;
        snd-msg 'Status: INACTIVE' ;
     when (status = PENDING) ;
       snd-msg 'Status: PENDING' ;
     other ;
       snd-msg 'Nieznany status' ;
   endsl;

 end-proc;

